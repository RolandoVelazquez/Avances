const gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    pug = require('gulp-pug'),
    uglify = require('gulp-uglify'),
    pump = require('pump'),
    browserSync = require('browser-sync').create();

//RutaCompilado
let RCompilado = './Compilado';

//Al compilar Pug 
let ruta_pug = './src-TT/Pug/*.pug';
let C_ruta_pug = RCompilado;

//Al compilas SASS
let ruta_sass = './src-TT/Sass/*.scss';
let C_ruta_sass = RCompilado + '/css/';

//Al compilar js
let ruta_js = './src-TT/Js/*.js'
let C_ruta_Js = RCompilado + '/js/';


//Manifesto
gulp.task('pug', () =>
    gulp.src(ruta_pug)
        .pipe(pug({
            pretty: true
        }))
        .pipe(gulp.dest(C_ruta_pug))
);

gulp.task('js', function (cb) {
    pump([
        gulp.src(ruta_js), //Ruta de la carpeta apuntando a los archivos `.js`
        uglify(), //Comprime los archivos `.js`
        gulp.dest(C_ruta_Js)//Carpeta donde se guardara el archivo `.js` comprimido
    ],
        cb
    );
});

gulp.task('sass', () =>
    gulp.src(ruta_sass)
        .pipe(sass({
            autputstyle: 'nested',
            sourceComments: true
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(C_ruta_sass))
);

gulp.task('default', () => {

    browserSync.init({
        server: RCompilado
    });

    gulp.watch('./Compilado/*.html').on('change', browserSync.reload);
    gulp.watch('./Compilado/css/*.css').on('change', browserSync.reload);
    gulp.watch('./Compilado/js/*.js').on('change', browserSync.reload);

    gulp.watch('./src-TT/Sass/**/*.scss', ['sass']);
    gulp.watch('./src-TT/Pug/**/*.pug', ['pug']);
    gulp.watch('./src-TT/Js/**/*.pug', ['js']);
})
